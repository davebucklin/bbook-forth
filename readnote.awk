BEGIN{
  data_dir="~/.bbook/"
  notes_path=data_dir "notes.rec"
  cmd = "recsel -t Note -e \"Contact=" id "\" \"" notes_path "\""
  while ((cmd | getline) > 0) {
    if ($0 == "")
      printf "%s\037%s\037%s\n", record["Date"], record["Time"], record["Content"]
    pos = index($0,":")
    key = substr($0,1,pos-1)
    value = substr($0,pos+2)
    gsub("\n","",value)
    record[key] = value
  }
  close(cmd)
  # Date Time Content
  printf "%s\037%s\037%s\n", record["Date"], record["Time"], record["Content"]
}
  
