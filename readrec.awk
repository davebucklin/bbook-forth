BEGIN{
  data_dir="~/.bbook/"
  contacts_path=data_dir "contacts.rec"
  cmd = "recsel -t Contact -e \"Id = " id "\" \"" contacts_path "\""
  while ((cmd | getline) > 0) {
    pos = index($0,":")
    key = substr($0,1,pos-1)
    value = substr($0,pos+2)
    gsub("\n","",value)
    record[key] = value
  }
  close(cmd)
  # Name Email Company Status Followup
  printf "%s\037%s\037%s\037%s\037%s", record["Name"], record["Email"], record["Company"], record["Status"], record["Followup"]
}
  
