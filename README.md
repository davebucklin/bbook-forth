# bbook-forth

A rendition of [bbook](https://gitlab.com/davebucklin/bbook) for gforth. Depends on GNU recutils and GNU coreutils and, of course, gforth.

# Installing

After cloning the repo, create the folder that will contain your contact information.

```
mkdir ~/.bbook
```

Copy the example recfiles to this folder.

```
cp *rec ~/.bbook
```

Also copy the awk helper scripts to this folder.

```
cp *awk ~/.bbook
```

# Usage

In the repository folder, type `gforth fbook.fs`. If bbook doesn't start automatically, type `start`.

To change the location of your datafiles, alter the datapath value in fbook.fs, as well as the data_dir values in all of the awk scripts.

## Functions

Contact Functions:

* **n** - Add a note to the current contact.
* **e** - Edit the current contact.
* **f** - Toggle followup flag.
* **h** - Show previous contact in the list.
* **j** - Select the next note in the list.
* **l** - Show next contact in the list.
* **k** - Select the previous note in the list.
* **a** - Toggle status active/inactive.
* **u** - Show previous page of notes.
* **d** - Show next page of notes.
* **Enter** - View the selected note. \<Space\> works, too.

Global Functions:

* **N** - Add a contact.
* **F** - Toggle followup list. Show only contacts with followup flag set.
* **A** - Toggle active filter. Show only contacts with Status = active.
* **/** - Search. Enter a blank value to clear a search.
* **q** - Exit bbook.
