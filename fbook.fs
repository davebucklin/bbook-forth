\ An attempt to recreate my bbook CRM in f! orth.
\ Probably not my best idea.
\ Initial plan is to continue using awk and recutils to manage the data.

decimal align

$7f constant bs                     \ backspace key
$0d constant enter                  \ enter key
$1b constant esc
$09 constant tab
1024 constant #idbuf                \ length of id buffer
variable #notes 0 #notes !          \ count of notes
10 cells constant #fieldstbl        \ length of fields pointer array
6 constant topnote                  \ y location of the first note line
8 cells constant #searchbuf         \ length of search buffer
create idbuf #idbuf allot           \ buffer for contact IDs
variable notestbl 0 notestbl !      \ pointer to notes table
create boldbuf 8 cells allot        \ terminal sequence for (un)bold text
variable 'outfile 0 'outfile !      \ outfile file pointer
variable index 0 index !            \ current contact ID index
variable 'rec 0 'rec !              \ contact data starting memory address
variable 'notes 0 'notes !          \ address of notes data
create fieldstbl #fieldstbl allot   \ fields pointer array
align
variable noteindex 0 noteindex !    \ current note index
variable detail 0 detail !          \ notes detail/list flag
variable followup 0 followup !      \ follow-up list flag
variable active -1 active !         \ active list flag
create searchbuf #searchbuf allot   \ buffer for search keyword
\ cell-counted strings/buffers
128 cells constant #cmdbuf          \ length of command buffer
create cmdbuf #cmdbuf allot         \ buffer for system commands
\ contact record fields
0 constant name:
1 constant email:
2 constant company:
3 constant status:
4 constant followup:
5 constant #fields
9 constant fieldx

: clearsearch ( -- )
  searchbuf #searchbuf erase ;

\ cell-counted string count
: $count ( addr -- addr u )
  dup cell + swap @ ;

\ cell-counted string append
: $append ( addr u >addr -- )
  dup >r over >r dup @ + cell + swap cmove r> r> +! ;

\ get lines and columns for the terminal
: cols ( -- ) form swap drop ;
: lines ( -- ) form drop ;
: datapath ( -- addr u ) s" " ;             \ use data files in pwd
: datapath ( -- addr u ) s" ~/.bbook/" ;
: len>range ( addr u -- loaddr hiaddr )   over + ;
: range>len ( loaddr hiaddr -- addr u )   over - ;

\
\ File Operations
\

: open ( -- )
  s" outfile" r/o open-file drop 'outfile ! ;

: read-line ( addr -- addr u )
  dup 1024 'outfile @ read-file if ." read error" exit then ;

: read ( - addr u )
  here dup begin read-line dup 1024 = while + repeat + range>len ;

: close ( -- )
  'outfile @ close-file drop ;

: fetch ( -- addr u )
  open read close ;

\
\ system command words
\

\ append string at addr u to counted string at >addr
\ if you try to stuff more than 255 characters into a byte-counted string,
\ this will corrupt the string
: append ( addr u >addr -- )
  dup >r over >r dup c@ + 1+ swap cmove r> r> +! ;

\ fill cmdbuf with spaces
: clearbuf    cmdbuf #cmdbuf $20 fill 0 cmdbuf ! ;
: .cmdbuf     cmdbuf $count type ;
: exec        cmdbuf $count system ;

\ move string at addr to the command buffer
: >cmdbuf ( addr u -- )
  clearbuf dup cmdbuf ! cmdbuf cell + swap cmove ;

\ append string at addr to command buffer
: +cmdbuf ( addr u -- ) cmdbuf $append ;

: seek ( char start-addr -- addr-of-char )
  begin dup c@ rot 2dup <> while -rot drop 1+ repeat 2drop ;

\
\ Output formatting helpers
\

boldbuf 2 cells + constant 'unbold
boldbuf 4 cells + constant 'eos
boldbuf 6 cells + constant 'el

\ get terminal bold and unbold sequences
s" tput bold >outfile" >cmdbuf exec fetch dup boldbuf c! boldbuf 1+ swap cmove
s" tput sgr0 >outfile" >cmdbuf exec fetch dup 'unbold c! 'unbold 1+ swap cmove
s" tput ed >outfile"   >cmdbuf exec fetch dup 'eos    c! 'eos    1+ swap cmove
s" tput el >outfile"   >cmdbuf exec fetch dup 'el     c! 'el     1+ swap cmove

\ formatting words
: hrule     0 topnote 1 - at-xy cols 1 - 0 do [char] - emit loop ;
: bold      boldbuf count type ;
: unbold    'unbold count type ;
: eos       'eos    count type ;
: el        'el     count type ;

\
\ Notes helper words
\

: blanknotes ( -- )
  0 topnote at-xy eos ;

\ translate note index into starting memory address
: note>count ( u -- addr len )
  2 cells * notestbl @ + 2@ ;

\ create an array of ( len addr ) string pointers
: index-notes ( addr u -- )
  here notestbl ! 0 #notes !
  len>range swap ( hi lo )
  begin
  2dup 10 + > while \ add a bit of a buffer in case there are a few extra bytes after the last note
    $0a over seek range>len
    2dup + 1+ -rot , ,
    1 #notes +!
  repeat 2drop align ;

: sub ( new find addr len -- )
  len>range 1+ swap do
    dup i c@ = if swap dup i c! swap then
  loop 2drop ;

\
\ Contact record management
\

: clearidbuf  idbuf #idbuf 0 fill ;
: >idbuf ( addr u -- ) idbuf swap cmove ;

\ translate index to id string address
: idx>contact ( u-idx -- addr u )
  0 idbuf
  \ set the starting address
  begin
  -rot 2dup > while
    rot dup c@ $0a = if swap 1+ swap then 1+
  repeat 2drop
  \ if there's not a null at this location, seek to the end
  dup c@ 0= if 1 else
    dup $0a swap seek range>len
  then ;

\ string location of contact id at current list index
: contact> ( -- addr u )
  index @ idx>contact ;

: get-ids ( -- addr u )
  clearidbuf
  0 index !
  s\" awk -vopts=\"-pId " >cmdbuf
  active @ if
    s\" -e \\\"Status='active'\\\" " +cmdbuf
  then
  followup @ if
    s\" -e \\\"Followup='yes'\\\" " +cmdbuf
  then
  s\" -t Contact\" -f " +cmdbuf
  datapath +cmdbuf
  s" buildlist.awk >outfile" +cmdbuf \ "
  exec fetch >idbuf ;

: field>len ( idx -- addr )
  2 cells * fieldstbl + ;

: field>addr ( idx -- addr )
  field>len cell + ;

: field ( u-idx -- addr u )
  field>len 2@ ;

\ Populate fieldstbl with the address and length of fields in the record
: index-rec ( -- )
  'notes @ 1- 'rec @ ( hi lo )
  0 >r \ field index
  begin
  #fields r@ > while
    $1f over seek range>len
    2dup + 1+ -rot r@ field>len !
    r> dup 1+ >r field>addr !
  repeat rdrop 2drop ;

: getcmd
  s\" awk -vid=\"" >cmdbuf contact> +cmdbuf
  s\" \" -f " +cmdbuf datapath      +cmdbuf
  s" readrec.awk >outfile"          +cmdbuf ; \ "

\ get contact record
: get-rec ( -- )
  'rec @ dp ! \ move data pointer to record origin
  getcmd exec fetch
  2dup + $1f swap c!  \ add an extra terminator
  1+ + dup aligned dup -rot range>len $20 fill \ align and fill
  dup 'notes !
  dp !  \ move the data pointer to the beginning of the notes area
  0 noteindex !
  index-rec ;

variable pagenum 0 pagenum !

\ Load notes data
: get-notes ( -- )
  'notes @ dup 0 <> if dp ! else drop then
  s\" awk -vid=\""                  >cmdbuf
  contact>                          +cmdbuf
  s\" \" -f "                       +cmdbuf
  datapath                          +cmdbuf
  s" readnote.awk | tac >outfile"   +cmdbuf \ "
  exec fetch 2dup + align dp !
  2dup index-notes bl $1f 2swap sub 0 pagenum ! ;

\ Display a contact and notes

\ print contact info, limit to first four fields
: fields ( -- )
  4 0 do fieldx i at-xy i field type loop ;

: labels
  ."    Name: " cr
  ."   Email: " cr
  ." Company: " cr
  ."  Status: " cr ;

: attr>followup
  followup @ if s" [list:Followup]" pad append then ;

: active?
  active @ if s" [list:Active]" pad append then ;

: followup? ( -- f )
  followup: field drop c@ [char] y = if s" [F]" pad append then ;

: attr>id
  s" [" pad append contact> pad append s" ]" pad append ;

: search?
  searchbuf count dup if s" [search:" pad append pad append s" ]" pad append else 2drop then ;

: attrs
  0 pad c! attr>followup active? search? followup? attr>id
  cols pad c@ - 0 at-xy pad count type ;

: header ( -- )
  labels fields attrs hrule ;

\
\ Display notes
\

: bold?
  noteindex @ = if bold then ;

: unbold?
  noteindex @ = if unbold then ;

: detaildate ( addr u -- addr u )
  ." Date: " over 11 type cr 11 - swap 11 + swap ;

: detailtime ( addr u -- addr u )
  ." Time: " over 6 type cr 6 - swap 6 + swap ;

: note-detail ( -- )
  noteindex @ blanknotes 0 topnote at-xy note>count detaildate detailtime type ;

: pagesize ( -- u )
  lines topnote - ;

: pagestart ( -- u )
  pagesize pagenum @ * ;

: pageend ( -- u )
  pagesize pagenum @ 1+ * 1- #notes @ 1- min ;

: (+page) ( -- )
  pageend #notes @ 1- < if 1 pagenum +! noteindex @ pagesize + #notes @ 1- min noteindex ! then ;

: (-page) ( -- )
  pagenum @ 0 > if pagenum @ 1- pagenum ! noteindex @ pagesize - noteindex ! then ;

\ shouldn't have to limit-check this here, but leaving it in for now
: drawnote ( u -- )
  dup #notes @ 1- > if drop exit then
  dup note>count cols 1 - min
  rot dup topnote + pagesize pagenum @ * - 0 swap at-xy
  dup bold? -rot type unbold? ;

: notes-list
  blanknotes #notes @ 0= if exit then
  pageend 1+ pagestart do i drawnote loop ;

: notes ( -- )
  detail @ if note-detail else notes-list then ;

\
\ Record navigation
\

: -index    index @ 1 - dup 0 < if drop 0 then index ! ;
: +index    index @ 1+ idx>contact drop @ 0 <> 1 and index +! ;
: display   page header notes ;
: refresh   get-rec get-notes display ;
: +rec      +index refresh ;
: -rec      -index refresh ;
: -noteindex noteindex @ 1 - dup 0 < if drop 0 then noteindex ! ;
: +noteindex noteindex @ #notes @ 1- < if 1 noteindex +! then ;
\ toggle detail mode
: details   detail @ invert detail ! notes ;

: ?page ( -- pagenum )
  noteindex @ pagesize / ;

: /page? ( -- f )
  ?page pagenum @ <> ;

: >page ( -- )
  ?page pagenum ! ;

defer prev
: redraw    detail @ if note-detail else noteindex @ dup prev drawnote drawnote then ;
: -note     -noteindex /page? if >page notes-list else ['] 1+ is prev redraw then ;
: +note     +noteindex /page? if >page notes-list else ['] 1- is prev redraw then ;

\
\ Editing a Note
\

: width     cols 2 / ;
: height    lines 2 / ;
: notepad   pad 18 + ;  \ add timestamp length to pad address

\ box-drawing characters
$2557 constant UR
$2554 constant UL
$255A constant LL
$255D constant LR
$2550 constant HB
$2551 constant VB

: drawbox ( x y width height -- )
  2over at-xy UL xemit
  over 2 - 0 do HB xemit loop UR xemit
  dup 2 - 0 do
    2over 1+ i + at-xy VB xemit
    over 2 - 0 do bl emit loop
    VB xemit
  loop
  1- rot + rot swap at-xy
  LL xemit 2 - 0 do HB xemit loop LR xemit ;

: notebox ( -- )
  width width 2 / - 1 -     \ x-position
  topnote 2 +               \ y-position
  width 2 +                 \ width
  height drawbox ;          \ height

\ time stamp builder
: year ( u -- )     0 <# # # # # #> pad swap cmove ;
: month ( u -- )    0 <# # # #> [char] - pad 4 + c! pad 5 + swap cmove ;
: day ( u -- )      0 <# # # #> [char] - pad 7 + c! pad 8 + swap cmove ;
: hour ( u -- )     0 <# # # #> bl pad 10 + c! pad 11 + swap cmove ;
: minute ( u -- )   0 <# # # #> [char] : pad 13 + c! pad 14 + swap cmove ;
: timestamp ( -- )  time&date year month day hour minute drop ;

: charcount ( addr' addr -- )
  swap - 0 lines at-xy el 0 lines at-xy . ;

\ print the char in the right place
: outchar ( addr' addr char -- addr' addr char )
  -rot 2dup swap - \ get the length
  width /mod topnote 3 + + swap \ y is len divided by input window width plus, say, topnote + 3
  cols width - 2 / + swap at-xy rot dup emit ; \ x is remainder plus term width - input window width /2

\ accept characters into a buffer until cr is seen
: edit ( -- addr u )
  notepad dup
  begin key ( pad' pad char )
  dup enter <> while
    -rot 2dup charcount rot
    dup bs = if \ if a bs is given, we point one char back, put a space there, and display a space there
      drop 2dup - 0 <> if 1 - then $20 outchar over c!
    else
      outchar over c! 1+ \ store the char, advance the pointer
    then
  repeat drop range>len ;

: savenote ( addr u -- )
  s\" recins -t Note -fDate -v\""   >cmdbuf
  pad 10                            +cmdbuf
  s\" \" -fTime -v\""               +cmdbuf
  pad 11 + 5                        +cmdbuf
  s\" \" -fContent -v\""            +cmdbuf
  ( note addr u )                   +cmdbuf
  s\" \" -fContact -v"              +cmdbuf
  contact> +cmdbuf      s"  "       +cmdbuf
  datapath                          +cmdbuf
  s" notes.rec"                     +cmdbuf
  s"  2>&1 >> logfile.txt"          +cmdbuf
  exec ;

: (newnote) ( -- )
  notebox timestamp edit
  dup 0 > if savenote else 2drop then ;

: newnote  (newnote) refresh ;

\
\ Search for a contact
\

: getsearch ( -- )
  clearsearch 0 lines 1 - at-xy ." Search: " pad #searchbuf accept pad swap searchbuf append ;

: query ( -- )
  clearidbuf
  s\" awk -vopts='-t Contact -p Id -q\""    >cmdbuf
  searchbuf count                           +cmdbuf
  s\" \"' -f "                              +cmdbuf \ "
  datapath                                  +cmdbuf
  s" buildlist.awk >outfile"                +cmdbuf
  exec fetch >idbuf ; \ "

: search ( -- )
  getsearch searchbuf count nip 0 > if
    0 index !
    query idbuf c@ 0= if
      ."  No match. Press any key." key drop clearsearch get-ids
    then
  else
    2drop get-ids
  then refresh  ;

\
\ Add a contact
\

\ populate pad with successive counted strings
: getfield ( start-addr -- end-addr )
  dup 1+ 64 accept 2dup swap c! + 1+ ;

\ capture name, email, company
: getcontact ( -- )
  pad fieldx 0 at-xy getfield
      fieldx 1 at-xy getfield
      fieldx 2 at-xy getfield drop ;

: lastid ( -- )
  0 begin dup idx>contact drop @ 0 <> while 1+ repeat 1 - index ! ;

: contacts.rec
  datapath                  +cmdbuf
  s" contacts.rec "         +cmdbuf ;

: (newcontact) ( -- )
  page labels hrule getcontact
  s\" recins -tContact -fName -v\""     >cmdbuf \ "
  pad count 2dup + -rot                 +cmdbuf
  s\" \" -fEmail -v\""                  +cmdbuf
  count 2dup + -rot                     +cmdbuf
  s\" \" -fCompany -v\""                +cmdbuf
  count                                 +cmdbuf
  s\" \" -fStatus -vactive -fFollowup -vno " +cmdbuf
  contacts.rec
  exec ; \ "

: newcontact
  (newcontact) get-ids lastid refresh ;

\
\ Line editor
\

\ Take an x position, y position, buffer address, max length?
\ Print the current value, place cursor under first character
\ if first char entered is an <enter>, bail and keep the current value
\ if and esc is given at any time, bail and keep the current value
\ if first char is not <enter>, blank the field and show typed value
\ a single space returned results in a null value

: default ( x y addr len -- x y addr len )
  2over at-xy 2dup type ;

: cout ( char x y -- )
  at-xy emit ;

: done? ( char -- f )
  dup enter = swap esc = or ;

: firstkey ( addr len -- addr char )
  key dup done? invert if swap spaces else -rot + swap then ;

defer dir
: cursor ( x y addr char -- x y addr )
  dup >r 2over cout r> over c! dir -rot swap dir swap 2dup at-xy rot ;

\ needs a lower bounds check
\ maybe implement delete
: ed ( x y start-addr len -- end-addr )
  2over at-xy firstkey
  begin
  dup done? invert while
  dup bs = if
    drop bl ['] 1- is dir cursor
  else
    ['] 1+ is dir cursor
  then
  key
  repeat drop -rot 2drop ;

: getstr ( x y addr len -- addr len )
  default >r dup 2swap rot r> ed range>len ;

: to-pad ( addr len -- addr len )
  tuck pad swap cmove pad swap ;

: to-here ( addr len -- addr len )
  here over 2swap here swap cmove 2dup 1+ + aligned dp ! ;

: editfield ( idx -- )
  dup >r field ( addr len ) to-pad fieldx r@ 2swap getstr
  dup 0 > if to-here r@ field>len 2! then rdrop ;

\ recset limits us to one action at a time
: recset
  s\" recset -tContact -e\"Id = "   >cmdbuf \ "
  contact>                  +cmdbuf
  s\" \""                   +cmdbuf ;

: savename ( -- )
  recset
  s\"  -fName -S\""         +cmdbuf
  name: field               +cmdbuf
  s\" \" "                  +cmdbuf \ "
  contacts.rec
  exec ; \ "

: savecompany ( -- )
  recset
  s\"  -fCompany "          +cmdbuf \ "
  company: field
  dup 0 > if
    s\" -S\""               +cmdbuf
    ( company-addr len )    +cmdbuf
    s\" \" "                +cmdbuf \ "
  else 2drop
    s" -d "                 +cmdbuf
  then
  contacts.rec
  exec ;

: saveemail ( -- )
  recset
  s\"  -fEmail "            +cmdbuf \ "
  email: field
  dup 0 > if
    s\" -S\""               +cmdbuf
    ( email-addr len )      +cmdbuf
    s\" \" "                +cmdbuf \ "
  else 2drop
    s" -d "                 +cmdbuf
  then
  contacts.rec
  exec ;

: savecontact ( -- )
  savename savecompany saveemail ;

: editcontact ( -- )
  3 0 do i editfield loop savecontact 0 0 at-xy ;

\ Toggle contact followup flag
: (/followup)
  s\" recset -tContact -e\"Id = "   >cmdbuf
  contact>                  +cmdbuf
  s\" \" -fFollowup -S"     +cmdbuf
  followup: field drop c@
  [char] y = if
    s" no "                 +cmdbuf
  else
    s" yes "                +cmdbuf
  then
  contacts.rec
  exec ;

: /followup (/followup) refresh ;

\ Toggle active status
: (/active)
  s\" recset -tContact -e\"Id = "    >cmdbuf
  contact>                      +cmdbuf
  s\" \" -fStatus -S"           +cmdbuf
  status: field drop c@
  [char] a = if
    s" inactive "               +cmdbuf
  else
    s" active "                 +cmdbuf
  then
  contacts.rec
  exec ;

: /active (/active) refresh ;

: /followup-list
  followup @ invert followup ! get-ids refresh ;

: /active-list
  active @ invert active ! get-ids refresh ;

: -page (-page) notes-list ;

: +page (+page) notes-list ;

\
\ Set up the vector table
\

32 cells constant #vtable
create vtable #vtable allot
vtable #vtable erase
variable vidx 0 vidx !

: vsym ( n -- addr )
  cells vtable + ;

: vxt ( n -- addr )
  16 + vsym ;

: addvector ( char xt -- )
  vidx @ vxt !
  vidx @ vsym !
  1 vidx +! ;

: vlookup ( char -- xt )
  0 swap 16 0 do
    dup i vsym @ = if
      drop 1+ i vxt @ leave
    then
  loop
  swap 0= if drop 0 then ;

: start
  clearsearch get-ids refresh
  begin key
  dup [char] q <> while
    vlookup dup 0 <> if execute else drop then
  repeat drop ;

char l ' +rec           addvector
char h ' -rec           addvector
char j ' +note          addvector
char k ' -note          addvector
char n ' newnote        addvector
char f ' /followup      addvector
char a ' /active        addvector
char e ' editcontact    addvector
char N ' newcontact     addvector
char F ' /followup-list addvector
char A ' /active-list   addvector
char / ' search         addvector
char u ' -page          addvector
char d ' +page          addvector
enter  ' details        addvector
bl     ' details        addvector

\
\ Diagnostic routines
\

: spit ( hi lo ) do i c@ emit loop ;
: .pad pad 128 dump ;
: timer ( xt -- ) utime drop swap execute utime drop swap - ;
: dumpnotestbl cr #notes @ 0 do i 2 cells * notestbl @ + 2@ . . cr loop ;

\ set the address for contact record data
\ this has to be the last thing so that the data pointer doesn't shift
\ dictionary items added after this may be overwritten by data read in

align here 'rec !
start bye
